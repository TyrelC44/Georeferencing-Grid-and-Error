
Georeferencing-Grid Tool

The Georeferencing-Grid Tool instantly generates a customized grid of shapefile points that can be imported into Google Earth to function as the reference marks used for georeferencing Google Earth screenshots.

