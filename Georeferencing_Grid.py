# Project
# Georeferencing-Grid Code

#Import needed modules (arcpy, os, math, csv, etc.)  
import arcpy
import os
import math
import csv


#INPUTS

# define screen ratio (screen height/screen width)
screen_ratio = 0.45

# define total number of rows 
col_count = 2

#list of the 3 extra point options
extra_points = 5

folder =r'E:\USU\Python_GIS\Project'
arcpy.env.workspace = r'E:\USU\Python_GIS\Project'
arcpy.env.workspace = os.path.join(folder, 'data')

#Get the Extent Shapefile that was made prior to running script     
ext_poly = arcpy.Describe('Teasdale_extent.shp') 
extent = ext_poly.extent




#Define the 4 individual extent values ( maximum X, minimum X, maximum Y, minimum Y) 
min_x, max_x, min_y, max_y = extent.XMin, extent.XMax, extent.YMin, extent.YMax

#Get extent width by finding the absolute value of the difference between the maximum X and minimum X value.
ext_wdth = abs(max_x - min_x)

#Get extent height by finding the absolute value of the difference between the maximum Y and minimum Y value.
ext_hght = abs(max_y - min_y)

#Get cell width by dividing the extent width by the total number of columns that the user first defines in the Python Toolbox
cell_wdth = ext_wdth/col_count

#Get cell height by multiplying the cell width by the screen ratio (0.45) or whatever ratio the user first defines in the Python Toolbox
cell_hght = cell_wdth*screen_ratio

#Get total number of rows by dividing extent height by cell height and then round number up to next whole number
row_count = int(math.ceil(ext_hght/cell_hght))

#Create a list of all rows ranging from zero to the total number of rows
row_list = range(0, row_count)
#Create a list of all columns ranging from zero to the total number of columns 
col_list = range(0, col_count)


#	Create a list of all points per row ranging from zero to the total number of points per row
col_points = range(0, row_count +1)
#	Create a list of all points per column ranging from zero to the total number of points per column
row_points = range(0, col_count +1)

##Define variables to help position the extra points within each cell

half_w = cell_wdth/2 #Get half the cell width
half_h = cell_hght/2 #Get half the cell height
third_w = cell_wdth/3 #Get one third the cell width
third_h = cell_hght/3 #Get one third the cell height
fourth_w = cell_wdth/4 #Get one fourth the cell width
fourth_h = cell_hght/4 #Get one fourth the cell height
third_wx2 =2*third_w #Get two thirds the cell width
third_hx2 = 2*third_h #Get two thirds the cell height
fourth_wx3 = 3*fourth_w #Get three fourths the cell width
fourth_hx3 = 3*fourth_h #Get three fourths the cell height




# OUTPUTS

# #2 extra points
if extra_points == 2:
    with open('pointgrid.csv', 'wb') as fp:
    
        writer = csv.writer(fp)
        writer.writerow(['FID','latitude','longitude','label'])
        k = 0
        col_name_list = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
        for n in col_points:
            for i in row_points:
                writer.writerow([k ,max_y-n*cell_hght, min_x + i*cell_wdth, k+1])
                k=k+1
        for n in row_list:  #2extra point1
            for i in col_list:
                writer.writerow([k ,(max_y-n*cell_hght)-half_h, (min_x + i*cell_wdth)+third_w, str(n+1) + str(col_name_list[i])+'1'])
                k=k+1
        for n in row_list: #2extra point 2
            for i in col_list:
                writer.writerow([k ,(max_y-n*cell_hght)-half_h, (min_x + i*cell_wdth)+third_wx2, str(n+1) + str(col_name_list[i])+'2'])
                k=k+1
           
# #4 extra points
elif extra_points == 4:
    with open('pointgrid.csv', 'wb') as fp:
        writer = csv.writer(fp)
        writer.writerow(['FID','latitude','longitude','label'])
        k = 0
        col_name_list = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
        for n in col_points:
            for i in row_points:
                writer.writerow([k ,max_y-n*cell_hght, min_x + i*cell_wdth, k+1])
                k=k+1
        for n in row_list:  #4extra point1
            for i in col_list:
                writer.writerow([k ,(max_y-n*cell_hght)-half_h, (min_x + i*cell_wdth)+fourth_w, str(n+1) + str(col_name_list[i])+'1'])
                k=k+1
        for n in row_list: #4extra point 4
            for i in col_list:
                writer.writerow([k ,(max_y-n*cell_hght)-half_h, (min_x + i*cell_wdth)+fourth_wx3, str(n+1) + str(col_name_list[i])+'4'])
                k=k+1
        for n in row_list:  #4extra point2
            for i in col_list:
                writer.writerow([k ,(max_y-n*cell_hght)-fourth_h, (min_x + i*cell_wdth)+half_w, str(n+1) + str(col_name_list[i])+'2'])
                k=k+1
        for n in row_list: #4extra point 3
            for i in col_list:
                writer.writerow([k ,(max_y-n*cell_hght)-fourth_hx3, (min_x + i*cell_wdth)+half_w, str(n+1) + str(col_name_list[i])+'3'])
                k=k+1
# #5 extra points                    
elif extra_points == 5:            
    with open('pointgrid.csv', 'wb') as fp:
        writer = csv.writer(fp)
        writer.writerow(['FID','latitude','longitude','label'])
        k = 0
        col_name_list = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
        for n in col_points:
            for i in row_points:
                writer.writerow([k ,max_y-n*cell_hght, min_x + i*cell_wdth, k+1])
                k=k+1
        for n in row_list:  #4extra point1
            for i in col_list:
                writer.writerow([k ,(max_y-n*cell_hght)-fourth_h, (min_x + i*cell_wdth)+fourth_w, str(n+1) + str(col_name_list[i])+'1'])
                k=k+1
        for n in row_list: #4extra point 4
            for i in col_list:
                writer.writerow([k ,(max_y-n*cell_hght)-fourth_h, (min_x + i*cell_wdth)+fourth_wx3, str(n+1) + str(col_name_list[i])+'4'])
                k=k+1
        for n in row_list:  #4extra point2
            for i in col_list:
                writer.writerow([k ,(max_y-n*cell_hght)-fourth_hx3, (min_x + i*cell_wdth)+fourth_w, str(n+1) + str(col_name_list[i])+'2'])
                k=k+1
        for n in row_list: #4extra point 5
            for i in col_list:
                writer.writerow([k ,(max_y-n*cell_hght)-fourth_hx3, (min_x + i*cell_wdth)+fourth_wx3, str(n+1) + str(col_name_list[i])+'5'])
                k=k+1
        for n in row_list: #4extra point 3
            for i in col_list:
                writer.writerow([k ,(max_y-n*cell_hght)-half_h, (min_x + i*cell_wdth)+half_w, str(n+1) + str(col_name_list[i])+'3'])
                k=k+1
                  
               
arcpy.CreateFeatureclass_management(out_path=arcpy.env.workspace, out_name= 'pointgrid.shp', geometry_type='POINT', spatial_reference = 'Teasdale_extent.shp')

arcpy.AddField_management('pointgrid.shp',"label", "TEXT")

with open('E:/USU/Python_GIS/Project/data/pointgrid.csv', 'rb') as fp:
    reader = csv.reader(fp)
    next(reader)
    
with arcpy.da.InsertCursor(in_table= 'pointgrid.shp', field_names=['FID','shape@y','shape@x', 'label']) as inserter, open('E:/USU/Python_GIS/Project/data/pointgrid.csv', 'rb') as fp:
    reader = csv.reader(fp)
    next(reader)
    
    for row in reader:
        yx = [row[1], row[2]]
        map(float,yx)
        inserter.insertRow(row)  


