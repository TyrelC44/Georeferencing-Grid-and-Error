# Project Tool
# Georeferencing-Grid Code

#Import needed modules
import arcpy
import os
import math
import csv

class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        self.label = "Demo Toolbox"
        self.alias = "dt"

        # List of tool classes associated with this toolbox
        self.tools = [Georeferencing_Grid]


class Georeferencing_Grid(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Georeferencing Grid"
        self.description = "Generate a Grid of Georeferencing Points"
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
        
         # first parameter
        param0 = arcpy.Parameter(
            displayName = 'Grid Extent Layer',
            name = 'ext_poly',
            datatype = 'GPFeatureLayer',
            parameterType = 'Required',
            direction = 'Input')

        # second parameter
        param1 = arcpy.Parameter(
            displayName = 'Number of Columns (26 max.)',
            name = 'col_count',
            datatype = 'GPLong',         
            parameterType = 'Required',
            direction = 'Input')
            
        # Set a value list of 1-26
        param1.filter.type = "ValueList"
        param1.filter.list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26]   
         
        # third parameter
        param2 = arcpy.Parameter(
            displayName = 'Number of Reference Points per Image',
            name = 'ref_points',
            datatype = 'GPLong',         
            parameterType = 'Required',     
            direction = 'Input')
            
        # Set a value list of 4, 6, 8 and 9
        param2.filter.type = "ValueList"
        param2.filter.list = [4, 6, 8, 9]
    
        # fourth parameter
        param3 = arcpy.Parameter(
            displayName = 'Screen Ratio (Monitor Height/Monitor Width)                                                          Recommended Ratio = 0.45 ',
            name = 'screen_ratio',
            datatype = 'GPDouble',         
            parameterType = 'Required',          
            direction = 'Input')
            
        param3.defaultEnvironmentName = 0.45
       
        # fith parameter
        param4 = arcpy.Parameter(
            displayName = 'Output CSV',
            name = 'output_csv',                 
            datatype = 'DEFile',             
            parameterType = 'Required',
            direction = 'Output')
            
        param4.filter.list = ['txt', 'csv']
        
        
        # sixth parameter
        param5 = arcpy.Parameter(
            displayName = 'Output Shapefile',
            name = 'output_shp',                 
            datatype = 'GPFeatureLayer',
            parameterType = 'Required',
            direction = 'Output')

        params = [param0, param1, param2, param3, param4, param5]
        return params


    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        
        #INPUT Perameters
        
        # Get the Extent Shapefile
        ext_poly = parameters[0].valueAsText
        
        # define total number of rows 
        col_count = parameters[1].value
        
        #list of the 5 point pattern options
        ref_points = parameters[2].value
        
        # define screen ratio (screen height/screen width)
        screen_ratio = parameters[3].value
        
        #OUTPUT Perameters
        
        #CSV output
        output_csv = parameters[4].valueAsText
        
        # SHAPEFILE output 
        output_shp = parameters[5].valueAsText
        
    
        
        #Get the extent of the Extent Shapefile    
        descibe_poly= arcpy.Describe(ext_poly) 
        extent = descibe_poly.extent
        
        #Define the 4 individual extent values ( maximum X, minimum X, maximum Y, minimum Y) 
        min_x, max_x, min_y, max_y = extent.XMin, extent.XMax, extent.YMin, extent.YMax
        
        #Get extent width by finding the absolute value of the difference between the maximum X and minimum X value.
        ext_wdth = abs(max_x - min_x)
        
        #Get extent height by finding the absolute value of the difference between the maximum Y and minimum Y value.
        ext_hght = abs(max_y - min_y)
        
        #Get cell width by dividing the extent width by the total number of columns that the user first defines in the Python Toolbox
        cell_wdth = ext_wdth/col_count
        
        #Get cell height by multiplying the cell width by the screen ratio (0.45) or whatever ratio the user first defines in the Python Toolbox
        cell_hght = cell_wdth*screen_ratio
        
        #Get total number of rows by dividing extent height by cell height and then round number up to next whole number
        row_count = int(math.ceil(ext_hght/cell_hght))
        
        #Create a list of all rows ranging from zero to the total number of rows
        row_list = range(0, row_count)
        #Create a list of all columns ranging from zero to the total number of columns 
        col_list = range(0, col_count)
        
        
        #	Create a list of all points per row ranging from zero to the total number of points per row
        col_points = range(0, row_count +1)
        #	Create a list of all points per column ranging from zero to the total number of points per column
        row_points = range(0, col_count +1)
        
        ##Define variables to help position the extra points within each cell
        
        half_w = cell_wdth/2 #Get half the cell width
        half_h = cell_hght/2 #Get half the cell height
        third_w = cell_wdth/3 #Get one third the cell width
        third_h = cell_hght/3 #Get one third the cell height
        fourth_w = cell_wdth/4 #Get one fourth the cell width
        fourth_h = cell_hght/4 #Get one fourth the cell height
        third_wx2 =2*third_w #Get two thirds the cell width
        third_hx2 = 2*third_h #Get two thirds the cell height
        fourth_wx3 = 3*fourth_w #Get three fourths the cell width
        fourth_hx3 = 3*fourth_h #Get three fourths the cell height
        
        
        # Write CSVs
        
        # # 6 Points
        if ref_points == 6:
            with open(output_csv, 'wb') as fp:
            
                writer = csv.writer(fp)
                writer.writerow(['FID','latitude','longitude','label'])
                k = 0
                col_name_list = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
                for n in col_points:
                    for i in row_points:
                        writer.writerow([k ,max_y-n*cell_hght, min_x + i*cell_wdth, k+1])
                        k=k+1
                for n in row_list:  #2extra point1
                    for i in col_list:
                        writer.writerow([k ,(max_y-n*cell_hght)-half_h, (min_x + i*cell_wdth)+third_w, str(n+1) + str(col_name_list[i])+'1'])
                        k=k+1
                for n in row_list: #2extra point 2
                    for i in col_list:
                        writer.writerow([k ,(max_y-n*cell_hght)-half_h, (min_x + i*cell_wdth)+third_wx2, str(n+1) + str(col_name_list[i])+'2'])
                        k=k+1
                   
        # # 8 points
        elif ref_points == 8:
            with open(output_csv, 'wb') as fp:
                writer = csv.writer(fp)
                writer.writerow(['FID','latitude','longitude','label'])
                k = 0
                col_name_list = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
                for n in col_points:
                    for i in row_points:
                        writer.writerow([k ,max_y-n*cell_hght, min_x + i*cell_wdth, k+1])
                        k=k+1
                for n in row_list:  #4extra point1
                    for i in col_list:
                        writer.writerow([k ,(max_y-n*cell_hght)-half_h, (min_x + i*cell_wdth)+fourth_w, str(n+1) + str(col_name_list[i])+'1'])
                        k=k+1
                for n in row_list: #4extra point 4
                    for i in col_list:
                        writer.writerow([k ,(max_y-n*cell_hght)-half_h, (min_x + i*cell_wdth)+fourth_wx3, str(n+1) + str(col_name_list[i])+'4'])
                        k=k+1
                for n in row_list:  #4extra point2
                    for i in col_list:
                        writer.writerow([k ,(max_y-n*cell_hght)-fourth_h, (min_x + i*cell_wdth)+half_w, str(n+1) + str(col_name_list[i])+'2'])
                        k=k+1
                for n in row_list: #4extra point 3
                    for i in col_list:
                        writer.writerow([k ,(max_y-n*cell_hght)-fourth_hx3, (min_x + i*cell_wdth)+half_w, str(n+1) + str(col_name_list[i])+'3'])
                        k=k+1
                        
        # # 9 points                    
        elif ref_points == 9:            
            with open(output_csv, 'wb') as fp:
                writer = csv.writer(fp)
                writer.writerow(['FID','latitude','longitude','label'])
                k = 0
                col_name_list = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
                for n in col_points:
                    for i in row_points:
                        writer.writerow([k ,max_y-n*cell_hght, min_x + i*cell_wdth, k+1])
                        k=k+1
                for n in row_list:  # point1
                    for i in col_list:
                        writer.writerow([k ,(max_y-n*cell_hght)-fourth_h, (min_x + i*cell_wdth)+fourth_w, str(n+1) + str(col_name_list[i])+'1'])
                        k=k+1
                for n in row_list: #point 4
                    for i in col_list:
                        writer.writerow([k ,(max_y-n*cell_hght)-fourth_h, (min_x + i*cell_wdth)+fourth_wx3, str(n+1) + str(col_name_list[i])+'4'])
                        k=k+1
                for n in row_list:  #4extra point2
                    for i in col_list:
                        writer.writerow([k ,(max_y-n*cell_hght)-fourth_hx3, (min_x + i*cell_wdth)+fourth_w, str(n+1) + str(col_name_list[i])+'2'])
                        k=k+1
                for n in row_list: #4extra point 5
                    for i in col_list:
                        writer.writerow([k ,(max_y-n*cell_hght)-fourth_hx3, (min_x + i*cell_wdth)+fourth_wx3, str(n+1) + str(col_name_list[i])+'5'])
                        k=k+1
                for n in row_list: #4extra point 3
                    for i in col_list:
                        writer.writerow([k ,(max_y-n*cell_hght)-half_h, (min_x + i*cell_wdth)+half_w, str(n+1) + str(col_name_list[i])+'3'])
                        k=k+1
        
        # # 4 points                    
        elif ref_points == 4:            
            with open(output_csv, 'wb') as fp:
                writer = csv.writer(fp)
                writer.writerow(['FID','latitude','longitude','label'])
                k = 0
                for n in col_points:
                    for i in row_points:
                        writer.writerow([k ,max_y-n*cell_hght, min_x + i*cell_wdth, k+1])
                        k=k+1   
            
            
                        
        # Create Point Shapefile               
        
        arcpy.CreateFeatureclass_management(out_path=os.path.dirname(output_shp), out_name= os.path.basename(output_shp), geometry_type='POINT', spatial_reference = ext_poly)
        
        arcpy.AddField_management(output_shp,"label", "TEXT")
        
        with open(output_csv, 'rb') as fp:
            reader = csv.reader(fp)
            next(reader)
            
        with arcpy.da.InsertCursor(in_table= output_shp, field_names=['FID','shape@y','shape@x', 'label']) as inserter, open(output_csv, 'rb') as fp:
            reader = csv.reader(fp)
            next(reader)
            
            for row in reader:
                yx = [row[1], row[2]]
                map(float,yx)
                inserter.insertRow(row)
        return
        
